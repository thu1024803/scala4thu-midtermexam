/**
  * Created by mark on 13/03/2017.
  */
import Questions._
import org.scalatest.FunSuite
/** 15%
  * 請用Tail Recursive寫出階乘函數fac(n)=n*(n-1)*...*2*1
  * ex:
  * fib(5)=120
  **/


class TailFacSpec extends FunSuite{
  test("tailFac(5,1)=120") {
    assert(tailFac(5,1)==120)

  }
  test("tailFac(7,1)=5040") {
    assert(tailFac(7,1)==5040)

  }
  test("tailFac(10,1)=3628800") {
    assert(tailFac(10,1)==3628800)

  }
  test("tailFac(10000000,1)") {
    assert(tailFac(10000000,1)==0)

  }


}
class MaxRecurSpec extends FunSuite{
  test("maxRecur(List(1,2,3))==3") {
    assert(maxRecur(List(1,2,3))==3)

  }
  test("maxRecur(List(-1,-2,-3))= -1") {
    assert(maxRecur(List(-1,-2,-3))== -1)

  }
  


}
class ReverseRecurSpec extends FunSuite{
  test("reverseRecur(List(1,2,3))==3") {
    assert(reverseRecur(List(1,2,3))==List(3,2,1))

  }
  test("reverseRecur(List(5,1,2,3))==List(3,2,1,5)") {
    
    assert(reverseRecur(List(5,1,2,3))==List(3,2,1,5))

  }



}


class WordCountSpec extends FunSuite{
  test("wordCount(List(a,b,b,c,c,c))==Map(a->1,b->2,c->3)") {
//    val words
    assert(wordCount(List("a","b","b","c","c","c"))==Map("a"->1,"b"->2,"c"->3))

  }




}

class AccumulatorSpec extends FunSuite{
  test("accumulator((1 to 5).toList)==List(1,3,6,10,15)") {
    //    val words
    assert(accumulator((1 to 5).toList)==List(1,3,6,10,15))

  }




}
class FlattenSpec extends FunSuite{
  test("flatten(List(List(1),List(2,2),List(3,3,3)))==List(1,2,2,3,3,3)") {
    //    val words
    assert(flatten(List(List(1),List(2,2),List(3,3,3)))==List(1,2,2,3,3,3))

  }




}



